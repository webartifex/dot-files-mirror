# gTile Configuration

This folder contains the file gTile.backup which can be used to recreate my
    keyboard shortcuts for basic window tiling using the gTile extension for
    GNOME 3.

Usually, I split my screen such that main working windows are more to the top
    and left. Smaller columns to the right and rows to the bottom are used for
    things like running a Jupyter notebook server, htop, or Git.

Using the dconf package, enter the following in your shell:

`dconf load /org/gnome/shell/extensions/gtile/ < gTile.backup`

The ASCII arts below visualize the shortcuts.
Note that as gTile is limited to 30 shortcuts, some grids are not defined in
    totality.

## Legend

General Format: "Combination of modifying keys" + "Number on numbers pad"

Modifying keys:

s := Shift

c := Ctrl

u := Super (i.e., "Windows" key on Lenovo keyboards)

a := Alt

Conceptually the shortcuts are set up such that the numbers on a numbers pad
    align with the general location of the block.

Example: "ua3" for Super + Alt + 3


## GNOME Built-ins

The following three shortcuts are built into GNOME 3 and work without gTile
    (using the basic arrow keys).

```
 _______________________
|                       |
|                       |
|          uUp          |
|                       |
|                       |
|_______________________|

 _______________________
|           |           |
|           |           |
|   uLeft   |   uRight  |
|           |           |
|           |           |
|___________|___________|

```

## Basic Grid Blocks

The following blocks are mainly used to fill up blank areas on the screen.

```
 _______________________
|           |           |
|    u7     |    u9     |
|___________|___________|
|           |           |
|    u1     |    u3     |
|___________|___________|

 _______________________
|           |   cu9     |
|___________|___________|
|           |   cu6     |
|___________|___________|
|    cu1    |   cu3     |
|___________|___________|

 _______________________
|       |       |  ua9  |
|_______|_______|_______|
|       |       |  ua6  |
|_______|_______|_______|
|  ua1  |  ua2  |  ua3  |
|_______|_______|_______|

```

## Symmetric Splits

Use these splits to, for example, work on two text files in parallel.
Fill in the lower row with one of the basic boxes from above.

```
 _______________________
|           |           |
|     u4    |    u6     |
|           |           |
|___________|___________|
|                       |
|_______________________|

```

Shift enlarges the main blocks downwards.

```
 _______________________
|           |           |
|           |           |
|    su4    |    su6    |
|           |           |
|___________|___________|
|____su1____|____su3____|

```

## Asymmetric Splits

Use these splits to work in one main window while monitoring / supporting the
    work flow (e.g., Git) in helper boxes.

Since the center column of the numbers pad is unused so far, "u5" starts a new
    main window while "u8" complements the empty area towards the right and
    "u2" towards the bottom.

```
 _______________________
|               |       |
|       u5      |  u8   |
|               |       |
|_______________|_______|
|       u2      |       |
|_______________|_______|

```

Again, Shift enlarges the main block downwards.

```
 _______________________
|               |       |
|               |       |
|      su5      |  su8  |
|               |       |
|_______________|_______|
|______su2______|_______|

```

Ctrl (instead of Shift) extends the asymmetric split to the full height.
"cu8" still complements the main block ("cu5") towards the right.

```
 _______________________
|               |       |
|               |       |
|      cu5      |  cu8  |
|               |       |
|               |       |
|_______________|_______|

```

"ca3" and "ca9" are a variant that split the right area in half.
This setting and the next are the only ones that do **not** use the Super key.

```
 _______________________
|               |       |
|               | ca9   |
|               |_______|
|               |       |
|               | ca3   |
|_______________|_______|

```

Finally, the subsequent "weird" block utilizes the entire screen if "su1" and
    "su3" are employed together with "su5" and the right column should be
    "splitted" (with "ua9").

```
 _______________________
|                       |
|                _______|
|               |       |
|               | ca6   |
|               |_______|
|_______________________|

```
